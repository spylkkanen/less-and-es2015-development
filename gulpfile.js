/*jshint globalstrict:true, devel:true, newcap:false */
/*global require */

/**
 * Build CSS and JavaScript using `gulp`.
 *
 * Main targets are: `js`, `css` and `watch`.
 *
 * Run with `--production` to get minified sources.
 */

"use strict";

var argv = require('yargs').argv,

    gulp       = require('gulp'),
    gutil      = require('gulp-util'),
    gulpif     = require('gulp-if'),

    source     = require('vinyl-source-stream'),
    buffer     = require('vinyl-buffer'),
    sourcemaps = require('gulp-sourcemaps'),
    // browserify = require('browserify'),
    watchify   = require('watchify'),
    // reactify   = require('reactify'),
    uglify     = require('gulp-uglify'),

    concat = require('gulp-concat'),  
    rename = require('gulp-rename'),  

    clean = require('gulp-clean'),

    less       = require('gulp-less'),
    minifyCSS  = require('gulp-minify-css'),

    header = require('gulp-header'), // assign the module to a local variable

    gulp = require('gulp'),
    babel = require('gulp-babel'),

    pkg = require('./package.json');


// Create file header banner.
var banner = ['/**',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version v<%= pkg.version %>',
  ' * @link <%= pkg.homepage %>',
  ' * @license <%= pkg.license %>',
  ' */',
  ''].join('\n');

// Directory where static files are found.
var staticStyleSheetDirectory = './css/',
    staticJavascriptDirectory = './js/';

// Remove generated css file.
gulp.task('css_clean', function () {
    return gulp.src(staticStyleSheetDirectory + 'app.css')
        .pipe(clean({force: true}))
        .pipe(gulp.dest('dist'));
});

// Build css from less.
gulp.task('css', ['css_clean'], function(){
    return gulp.src(staticStyleSheetDirectory + 'app.less')
        .pipe(less()) // less conversion.
        .pipe(gulpif(argv.production, minifyCSS({keepBreaks:false}))) // minify stylesheet files.
        .pipe(header(banner, { pkg : pkg } )) // add file header banner text.
        .pipe(gulp.dest(staticStyleSheetDirectory));
});

// Remove generated javascript file.
gulp.task('js_clean', function () {
    return gulp.src(staticJavascriptDirectory + 'app.js')
        .pipe(clean({force: true}))
        .pipe(gulp.dest('dist'));
});

// Remove generated javascript source map file.
gulp.task('js_map_clean', function () {
    return gulp.src(staticJavascriptDirectory + '*.map')
        .pipe(clean({force: true}))
        .pipe(gulp.dest('dist'));
});

// Build js file (concat and uglify)
gulp.task('js', ['js_map_clean', 'js_clean'], function() {
    return gulp.src(staticJavascriptDirectory + '/**/*.js')
        .pipe(babel()) // ES2015 conversion.
        .pipe(concat('app.js')) // concat all js files to single file.
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(gulpif(!argv.production, sourcemaps.write('./'))) // writes .map file // loads map from browserify file
        .pipe(gulpif(argv.production, uglify())) // minify javascript files.
        .pipe(header(banner, { pkg : pkg } )) // add file header banner text.
        .pipe(gulp.dest(staticJavascriptDirectory));
});

// gulp.task('csswatch', function () {
//     gulp.watch(staticStyleSheetDirectory, ['css']);
// });

// gulp.task('jswatch', function () {
//     gulp.watch(staticJavascriptDirectory, ['less']);
// });

// gulp.task('watch', ['jswatch', 'csswatch']);
gulp.task('default', ['js', 'css']);
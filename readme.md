## npm package installation ##
Install's packages defined in package.json.

```
npm install
```


## run package.json scripts ##
```
npm run dev
npm run prod
npm run clean
```
or run scripts without package.json
```
gulp js css
gulp js css --production
gulp js_clean js_map_clean css_clean
```

## links ##
[https://blog.jayway.com/2014/03/28/running-scripts-with-npm/](https://blog.jayway.com/2014/03/28/running-scripts-with-npm/)  
[https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/)  
[https://css-tricks.com/gulp-for-beginners/](https://css-tricks.com/gulp-for-beginners/)  
[https://coderwall.com/p/zpquzq/gulpfile-for-projects-with-less-browserify-and-react](https://coderwall.com/p/zpquzq/gulpfile-for-projects-with-less-browserify-and-react)  
[http://lesscss.org/](http://lesscss.org/)  
[http://www.creativebloq.com/css3/create-modular-and-scalable-css-9134351](http://www.creativebloq.com/css3/create-modular-and-scalable-css-9134351)  
[https://css-tricks.com/design-systems-building-future/](https://css-tricks.com/design-systems-building-future/)  
[http://www.creativebloq.com/web-design/manage-large-scale-web-projects-new-css-architecture-itcss-41514731/2](http://www.creativebloq.com/web-design/manage-large-scale-web-projects-new-css-architecture-itcss-41514731/2)  
[https://blog.jayway.com/2014/03/28/running-scripts-with-npm/](https://blog.jayway.com/2014/03/28/running-scripts-with-npm/)  
[http://verekia.com/less-css/dont-read-less-css-tutorial-highly-addictive/](http://verekia.com/less-css/dont-read-less-css-tutorial-highly-addictive/)  


## npm configuration ##
```
npm config list
npm config edit
 => edit config paths and save.
    prefix = "D:\\node\\npm"
    cache = "D:\\node\\npm-cache"
    globalconfig = "D:\\node\\npm\\etc\\npmrc"
    globalignorefile = "D:\\node\\npm\\etc\\npmignore"
    init-module = "D:\\node\\.npm-init.js"
    tmp = "D:\\node\\Temp"
    userconfig = "D:\\node\\.npmrc"
```
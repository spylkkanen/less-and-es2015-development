/**
 * aspnet - Test applications
 * @version v1.2.3
 * @link http://www.test_dummy_domain.fi
 * @license GPL
 */
"use strict";

var _person = require("./person");

var _person2 = _interopRequireDefault(_person);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var person = new _person2.default("Ram", "Kulkarni"); /*
                                                        Application javascript file
                                                      */

// var app = function() {
//     var texts = [
//         { "savebutton": "Save", "method": this.saveClick },
//         { "cancelbutton": "Cancel", "method": this.cancel }];

//     var cancel = function() {
//         // Ajax post.
//     }

//     var save = function() {
//         // Ajax post.
//     }
// }

document.getElementById("nameSpan").innerHTML = person.getFirstName() + " " + person.getLastName();
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Person = function () {
    function Person(firstName, lastName) {
        _classCallCheck(this, Person);

        this.firstName = firstName;
        this.lastName = lastName;
    }

    _createClass(Person, [{
        key: "getFirstName",
        value: function getFirstName() {
            return this.firstName;
        }
    }, {
        key: "getLastName",
        value: function getLastName() {
            return this.lastName;
        }
    }]);

    return Person;
}();

exports.default = Person;
"use strict";

/*
  Base javascript file
*/

var menu = function menu() {
    var menuItems = [{ "Id": 1, "Name": "App 1" }, { "Id": 2, "Name": "App 2" }, { "Id": 3, "Name": "App 3" }];

    menuItems.forEach(function (menuItem) {
        // Add items to DOM.
    }, this);
};
//# sourceMappingURL=app.js.map
